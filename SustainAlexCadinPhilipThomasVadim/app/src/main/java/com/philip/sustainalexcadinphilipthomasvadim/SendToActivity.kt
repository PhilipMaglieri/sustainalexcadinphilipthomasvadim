package com.philip.sustainalexcadinphilipthomasvadim

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.ContactsContract
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.util.Log
import android.widget.Toast


class SendToActivity : AppCompatActivity() {

    private var co2:String? = null

    private lateinit var nameInput:EditText
    private lateinit var emailInput:EditText
    private lateinit var subjectInput:EditText
    private lateinit var textInput:EditText

    private lateinit var emailText:String

    //request number to identify request after user chooses to deny or allow contact permission
    private val MY_PERMISSIONS_REQUEST_READ_CONTACTS:Int = 1

    /**
     * onCreate of the activity. We just get some of the widgets from the layout
     * that we need to access in this script.
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_to)

        //widgets from the activity we need to access
        nameInput = findViewById<EditText>(R.id.recipient_name)
        emailInput = findViewById<EditText>(R.id.recipient_email)
        subjectInput = findViewById<EditText>(R.id.email_subject)
        textInput = findViewById<EditText>(R.id.email_text)

        val intent = intent
        val trees = intent.getDoubleExtra("trees", 0.0)
        val co2 = intent.getDoubleExtra("co2",0.0)
        val mode = intent.getStringExtra("mode")
        emailText = textInput.getText().toString() + " $trees " + getString(R.string.email_default_message_2) + " $co2 " + getString(R.string.email_default_message_3) + " $mode"
        textInput.setText(emailText)
    }

    /**
     * This is the event handler for the send_email_button.
     * It gets all the email content from the editTexts in the
     * view and starts an implicit intent
     * @param view
     */
    fun sendEmail(view: View){

        val intent = Intent(Intent.ACTION_SEND).apply {
            data = Uri.parse("mailto:")
            type = "*/*"
            //the class edit text returns an editable instead of a string
            putExtra(Intent.EXTRA_EMAIL, arrayOf(emailInput.getText().toString()))
            putExtra(Intent.EXTRA_SUBJECT, subjectInput.getText().toString())
            putExtra(Intent.EXTRA_TEXT, textInput.getText().toString())
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    /**
     * this is the event handler for the find_email_button.
     * If the access contacts permission has not been allowed yet, then it
     * prompts the user to allow or deny. If already allowed then this method will
     * call getContactsEmailByName
     * @param view
     */
    fun findRecipient(view: View){

        //if the user hasn't allowed the permission to check into contacts then prompt the authorization
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), MY_PERMISSIONS_REQUEST_READ_CONTACTS)
        }
        //else the user has allowed the permission to check into contacts
        else
        {
            getContactEmailByName()
        }

    }

    /**
     * override the request permission result. This method i called
     * after the user is prompted to allow or deny permission to access its contacts.
     * If the user allows than getContactEmailByName is called, else a toast will appear
     * with a message
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            this.MY_PERMISSIONS_REQUEST_READ_CONTACTS -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContactEmailByName()
                } else {
                    Toast.makeText(this,getString(R.string.contact_permission_denied), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    /**
     * this function, gets the name from the name_input editText widget and looks up
     * if there is a contact with the same name as the name provided. It then looks up
     * at all the contacts emails and tries to find one that correspond with the contact
     * we already retrieved. After finding a first match we break the loop and change the
     * email to now have the information we retrieved.
     * (If couldn't retrieve email a toast pops up)
     * (It is implied that the permission to access contacts has been granted
     * before calling this method)
     */
    private fun getContactEmailByName(){
        var email: String? = null
        val contentResolver = contentResolver

        //this will make getting the name of a contact work with any android version
        val displayName = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        else
            ContactsContract.Contacts.DISPLAY_NAME

        //where statement
        val selection = "lower($displayName) = ?"
        val selectionArguments = arrayOf(nameInput.text.toString().decapitalize())

        val cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, selection, selectionArguments, null)
        if (cursor!!.count > 0) {
            //looking at every single contact
            contact_loop@while (cursor.moveToNext()) {
                //getting the user contact id to check email
                val id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                //checks the emails to see if any have the user contact id
                val emailCursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", arrayOf(id), null)
                while (emailCursor!!.moveToNext()) {
                    email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                    Log.e("Email", email)
                    break@contact_loop//after we find the first email of a contact with the name inputed in we break the loop
                }
                emailCursor.close()

            }
        }
        cursor.close()

        if(email == null)
        {
            Log.e(TAG, "email retrieval error")
            val errorToast = Toast.makeText(this,getString(R.string.email_error),Toast.LENGTH_SHORT)
            errorToast.show()
        }
        else
        {
            //setting up the default email message with the name of the contact integrated and adding the email to the email field
            emailInput.setText(email)
            textInput.setText(getString(R.string.hi) + " " + nameInput.getText() + " " + emailText)
        }
    }

    companion object {
        //Logging
        private val TAG = "SendToActivity"
    }
}



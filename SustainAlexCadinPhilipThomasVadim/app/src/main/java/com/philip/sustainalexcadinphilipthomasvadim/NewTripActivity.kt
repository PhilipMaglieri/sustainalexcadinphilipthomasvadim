package com.philip.sustainalexcadinphilipthomasvadim

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup

class NewTripActivity : AppCompatActivity() {

    private lateinit var tripName : EditText

   /** private lateinit var fromLatitude : EditText
    private lateinit var fromLongitude : EditText
    private lateinit var toLatitude : EditText
    private lateinit var toLongitude : EditText
    private lateinit var distance : EditText
    private lateinit var co2 : EditText
    private lateinit var date : EditText
    private lateinit var time : EditText **/
    private lateinit var reason : EditText

    private lateinit var radioGroupMode : RadioGroup
    private lateinit var radioGroupDestination : RadioGroup

    private var travelMode : String? = null
    private var destination : String? = null

    private var carpool : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_trip)

        radioGroupMode = findViewById(R.id.radio_group)
        radioGroupDestination = findViewById(R.id.radio_group2)

        tripName = findViewById(R.id.tripNameTextView)
        reason = findViewById(R.id.reasonTextView)

        val button = findViewById<Button>(R.id.button_save)
        button.setOnClickListener {

            val selectedMode = findViewById<RadioButton>(radioGroupMode.checkedRadioButtonId)

            when(selectedMode.id){
                R.id.travel_mode_car -> travelMode="car"
                R.id.travel_mode_carpool -> {travelMode="car"; carpool = "3"}
                R.id.travel_mode_public_transit -> travelMode="publicTransport"
                R.id.travel_mode_walk_bike -> travelMode = "walk/bike"
            }

            val selectedDestination = findViewById<RadioButton>(radioGroupDestination.checkedRadioButtonId)
            when(selectedDestination.id){
                R.id.homeChoice -> destination="home"
                R.id.schoolChoice -> destination="school"
            }

            val replyIntent = Intent()
            if (TextUtils.isEmpty(tripName.text) || TextUtils.isEmpty(reason.text) ) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                val name = tripName.text.toString()
                val mode = travelMode
                val dest = destination
                val reas = reason.text.toString()


                val details = arrayListOf(name,mode,dest,reas,carpool)
                replyIntent.putExtra(DETAILS,details)
                /**Extra(NAME, name)
                replyIntent.putExtra(MODE, mode)
                replyIntent.putExtra(DESTINATION, dest)
                replyIntent.putExtra(REASON, reas)**/

                setResult(Activity.RESULT_OK, replyIntent)
            }
            finish()
        }
    }

    companion object {
        const val DETAILS = "details"
        const val NAME = "name"
        const val MODE = "mode"
        const val DESTINATION = "destination"
        const val REASON = "reason"
    }
}

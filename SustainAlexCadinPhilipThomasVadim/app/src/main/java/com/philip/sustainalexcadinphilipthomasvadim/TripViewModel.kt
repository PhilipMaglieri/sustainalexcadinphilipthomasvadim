package com.philip.sustainalexcadinphilipthomasvadim

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class TripViewModel(application: Application) : AndroidViewModel(application) {
    private val repository : TripRepository
    /**private**/ val allTrips : LiveData<List<Trip>>

    init{
        val tripDao = TripRoomDatabase.getDatabase(application, viewModelScope).tripDao()
        repository = TripRepository(tripDao)
        allTrips = repository.allTrips
    }


    fun insert (trip : Trip) = viewModelScope.launch {
        repository.insert(trip)
    }
}
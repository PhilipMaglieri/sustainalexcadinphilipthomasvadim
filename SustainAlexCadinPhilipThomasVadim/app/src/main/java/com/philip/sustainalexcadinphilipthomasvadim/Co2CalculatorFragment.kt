package com.philip.sustainalexcadinphilipthomasvadim

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.google.android.gms.location.*
import org.json.JSONObject
import java.io.*
import java.lang.Math.ceil
import java.math.RoundingMode
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.DecimalFormat
import javax.net.ssl.HttpsURLConnection

class Co2CalculatorFragment : DialogFragment() {

    private lateinit var sharedPref:SharedPreferences

    private lateinit var viewOfLayout: View
    private lateinit var resultText:TextView
    private lateinit var radioGroup: RadioGroup
    private lateinit var resultTextExtra:TextView
    private lateinit var sendToButton:Button

    private var trees:Double? = null
    private var trip:Trip = Trip("","","","","","",0.0,0.0,"","","")

    /**
     * onCreate is used to set the style of the dialog and getting the school location
     */
    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, android.R.style.Theme_DeviceDefault_Light_Dialog_MinWidth)

        sharedPref = this.activity?.getSharedPreferences("user_info",Context.MODE_PRIVATE)!!
        var schoolLocation= sharedPref?.getString("school", null)!!.split(",")
        trip.toLongitude = schoolLocation[1]
        trip.toLatitude = schoolLocation[0]
    }

    /**
     * onCreateView i get a referennce to the ui components i need to access to adding the event listeners
     * in code since is the only way to add them in a fragment
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreate(savedInstanceState)
        //UI
        viewOfLayout = inflater!!.inflate(R.layout.activity_co2_fragment, container, false)
        radioGroup = viewOfLayout.findViewById(R.id.radio_group)
        resultText = viewOfLayout.findViewById(R.id.calculation_result)
        resultTextExtra = viewOfLayout.findViewById(R.id.result_textView_extra)
        resultTextExtra.visibility = View.INVISIBLE
        dialog?.setTitle(getString(R.string.co2_calculator))

        val calculateButton = viewOfLayout.findViewById<Button>(R.id.calculate_button)
        calculateButton.setOnClickListener{ view -> calculateCO2(view)}

        sendToButton = viewOfLayout.findViewById<Button>(R.id.send_to)
        sendToButton.setOnClickListener{view -> showSendToActivity(view)}
        sendToButton.visibility = View.INVISIBLE

        return viewOfLayout
    }

    /**
     * event handle for the send to button.
     * goes to send to activity
     * @param view Button
     */
    fun showSendToActivity(view: View){
        val intent = Intent(this.activity, SendToActivity::class.java)
        intent.putExtra("trees", trees)
        intent.putExtra("co2", trip.co2)
        intent.putExtra("mode", trip.travelMode)
        startActivity(intent)
    }

    /**
     * event handler for calculate button. Gets the user location and calls
     * function to calculate amount of co2 emitted by calling the api
     */
    fun calculateCO2(view: View){
        sendToButton.visibility = View.INVISIBLE
        resultTextExtra.visibility = View.VISIBLE

        //getting the travel mode
        val selectedButton =
            viewOfLayout.findViewById<RadioButton>(radioGroup.checkedRadioButtonId) ?: return
        resultText.text = getString(R.string.loading)
        when(selectedButton.id){
            R.id.travel_mode_car_gas -> trip.travelMode="car(gas)"
            R.id.travel_mode_car_diesel -> trip.travelMode ="car(diesel)"
            R.id.travel_mode_carpool->trip.travelMode="car(gas)"
            R.id.travel_mode_public_transit->trip.travelMode="publicTransport"
            R.id.travel_mode_walk_bike-> {
                trip.travelMode="walk/bike"
                trip.co2=0.0
                displayCo2EmissionsForNoCo2ModeOfTransportation()
                return
            }
        }

        //checking if the location permission has been approved
        val hasLocationPermission = ContextCompat.checkSelfPermission( this.activity as Activity, android.Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED
        if(hasLocationPermission){
            Log.d("location", "success")
            getCO2EmissionsUsingLocation()
        } else{
            //request location permission if not allowed already
            requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),MY_PERMISSIONS_REQUEST_GET_LOCATION)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        Log.d(TAG, "called request permissions result")
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_GET_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "location : RequestPermissionResult success")
                    getCO2EmissionsUsingLocation()
                } else {
                    Log.d(TAG, "location : denied")
                    Toast.makeText(this.activity,getString(R.string.location_error), Toast.LENGTH_SHORT).show()
                    resultText.text = ""
                }
            }
        }
    }

    /**
     * gets the latest update for the location and calls function to call API
     * once successful
     */
    private fun getCO2EmissionsUsingLocation(){

        Log.d(TAG, "Getting device location")

        /*
            Callback function used to check for device location updates.
            Calls the Open Co2 API when it gets the device location.
        */
        val locationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    Log.d(TAG, "Got location")
                    startThreadToGetCo2FromAPI()
                }
            }
        }

        val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.activity as Activity)

        fusedLocationClient.lastLocation.addOnSuccessListener { location : Location? ->
            if(location != null){

                Log.d(TAG, "Got location")
                trip.fromLatitude = location.latitude.toString()
                trip.fromLongitude = location.longitude.toString()
                startThreadToGetCo2FromAPI()

            } else{
                Log.e(TAG, "Couldn't get location")
                Toast.makeText(this.activity,getString(R.string.location_error), Toast.LENGTH_SHORT).show()
                // if location was null, a location update is requested from the framework.
                fusedLocationClient.requestLocationUpdates(
                    LocationRequest(),
                    locationCallback,
                    Looper.getMainLooper())
            }
        }
    }



    /**
     * checks if there is an internet connection. If there is
     * starts thread to get authecationToken and CO2emissions
     */
    private fun startThreadToGetCo2FromAPI(){
        // HTTPURLConnection will not redirect http->https https->http
        // so check, swap or add http
        Log.d(TAG, "url$LOGINURL")

        // first check to see if we can get on the network
        val connMgr = this.activity?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo

        if (networkInfo != null && networkInfo.isConnected) {

            // invoke the AsyncTask to do the dirtywork.
            //this will get for us the authentication token
            //when we receive it we do another call to get the emissions (see onPostExecute)
            DownloadCo2Data().execute(LOGINURL, AUTHENTICATIONJSON, "POST", "")
        } else {
            Log.e(TAG,"no internet connection" )
            Toast.makeText(this.activity,getString(R.string.wifi_error), Toast.LENGTH_SHORT).show()
            resultText.text = ""
        }
    }

    private fun requestTripInfoFromAPI(authToken : String)
    {
        //extra parameters for car (engine & consumption)
        var tripCallParemeters: String
        if(trip.travelMode == "car(gas)")
            tripCallParemeters = "?fromlatitude=${trip.fromLatitude}&fromlongitude=${trip.fromLongitude}&tolatitude=${trip.toLatitude}&tolongitude=${trip.toLongitude}&mode=car&engine=$FIXEDGASOLINEENGINE&consumption=$FIXEDCONSUMPTION"
        else if(trip.travelMode == "car(diesel)")
        {
            tripCallParemeters = "?fromlatitude=${trip.fromLatitude}&fromlongitude=${trip.toLongitude}&tolatitude=${trip.toLatitude}&tolongitude=${trip.toLongitude}&mode=car&engine=$FIXEDDIESELENGINE&consumption=$FIXEDCONSUMPTION"
        }
        else
        {
            tripCallParemeters = "?fromlatitude=${trip.fromLatitude}&fromlongitude=${trip.fromLongitude}&tolatitude=${trip.toLatitude}&tolongitude=${trip.toLongitude}&mode=${trip.travelMode}"
        }

        //starts tripCO2 thread
        DownloadTripData().execute(TRIPINFOURL, tripCallParemeters, "GET", authToken)
        Log.d(TAG, "logTripInfo called")
    }

    private fun getTripC02FromAPIResponse(result: JSONObject): Double?
    {
        trip.co2 = result["co2emissions"].toString().toDouble()

        //in the case of carpool we divide the result and formatted to keep 3 decimals
        Log.d(TAG,"Called decimal format")
        if(radioGroup.checkedRadioButtonId == R.id.travel_mode_carpool) {
            var decimalFormat:DecimalFormat =  DecimalFormat("###.0000")
            decimalFormat.roundingMode = RoundingMode.CEILING
            trip.co2 = decimalFormat.format(trip.co2 / 3).toDouble()
        }
        Log.d(TAG,trip.co2.toString())
        return trip.co2
    }

    private fun displayCo2Results(co2Emission: Double, trees : Double)
    {
        var resultString = getString(R.string.co2_calculation_response)+" ${trip.co2} "+getString(R.string.co2_calculation_response_2)+ " $trees "
            resultString += getString(R.string.trees)

        resultText.text = resultString
    }

    private fun calculateHowManyTreesToPlantToOffsetCo2(Co2Emissions: Double): Double
    {
        var decimalFormat:DecimalFormat =  DecimalFormat("###.###")
        //one mature tree absorbs carbon dioxide at a rate of 48 lbs/year
        //48lbs = 21.7724kg
        var result = decimalFormat.format(trip.co2 / TREECO2ABSORPTIONINKG).toDouble()

        return result
    }

    private fun displayCo2EmissionsForNoCo2ModeOfTransportation()
    {
        trip.co2 = 0.0
        trees = 0.0
        displayCo2Results(trip.co2, trees!!)
        sendToButton.visibility = View.VISIBLE
    }

    /**
     * thread that gets the authentication token. Once it gets the token, it
     * starts another thread to calculate the CO2 emissions
     * @return AsyncTask
     */
    private inner class DownloadCo2Data : AsyncTask<String, Void, JSONObject>() {

        // onPreExecute log some info
        // runs in calling thread (in UI thread)
        override fun onPreExecute() {
            Log.d(TAG, "onPreExecute()")

        }

        // onPostExecute calls the calculation thread after getting
        //the authentication token
        // runs in calling thread (in UI thread)
        override fun onPostExecute(result: JSONObject) {

            Log.d(TAG,result.toString())
            //check if authentication failed
            if(result.has("error"))
            {
                resultText.text = getString(R.string.co2_error)
                return
            }
            val authToken = result["access_token"].toString()

            Log.d(TAG, "onPostExecute result: " + authToken)

            requestTripInfoFromAPI(authToken)
        }

        // runs in background (not in UI thread)
        override fun doInBackground(vararg params: String): JSONObject {
            // params comes from the execute() call: params[0] is the url.

            try {
                return downloadData(*params)
            } catch (e: IOException) {
                Log.d(TAG, e.stackTrace.toString())
                Log.d(TAG,"IOException")
                return getErrorJSON(R.string.co2_error)
            }

        }
    }

    /**
     * The thread that we call after getting the token, to calculate
     * the trip co2 emissions using the API.
     * @return AsyncTask
     */
    private inner class DownloadTripData : AsyncTask<String, Void, JSONObject>() {
        // onPreExecute log some info
        // runs in calling thread (in UI thread)
        override fun onPreExecute() {
            Log.d(TAG, "onPreExecute()")

        }

        //RESPONSE
        //call after we make the call to the API
        //here is where we get the response
        @SuppressLint("SetTextI18n")
        override fun onPostExecute(result: JSONObject) {
            Log.d(TAG, result.toString())
            if(result.has("error"))
            {
                resultText.text = result["error"].toString()
                return
            }

            trip.co2 = getTripC02FromAPIResponse(result)!!
            trip.distance = result["distance"].toString().toDouble()
            trip.time = result["traveltime"].toString()
            trees = calculateHowManyTreesToPlantToOffsetCo2(trip.co2)

            displayCo2Results(trip.co2, trees!!)
            sendToButton.visibility = View.VISIBLE
        }

        // runs in background (not in UI thread)
        override fun doInBackground(vararg params: String): JSONObject {
            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadData(*params)
            } catch (e: IOException) {

                return getErrorJSON(R.string.co2_error)
            }

        }
    }

    /**
     * Makes http calls depending on the input and returns response as JSON
     * @param params
     * @return JSONObject json
     */
    @Throws(IOException::class)
    private fun downloadData(vararg params: String): JSONObject {
        var instream: InputStream? = null
        val out: OutputStream
        var contentAsString : String
        val response: Int
        val url: URL
        val urlstring : String

        var conn: HttpsURLConnection? = null

        // this should be moved, creating & parsing the url doesn't need to be done in the background
        //param[0]  URL
        //param[1]  json or get parameter (ingr)
        //param[2]   GET or POST
        var bytes : ByteArray = "".toByteArray(charset("UTF-8"))
        var http = params[2]
        if (http == "GET") {
            urlstring = params[0] + params[1]
        } else {
            urlstring = params[0]
            bytes = params[1].toByteArray(charset("UTF-8"))
        }

        try {
            url = URL(urlstring)
            Log.d(TAG, "download type "+http+" url "+url)
        } catch (e: MalformedURLException) {
            Log.d(TAG, "malformedurl")
            Log.d(TAG, e.stackTrace.toString())
            Log.d(TAG, e.message.toString())
            return getErrorJSON(R.string.co2_error)
        }

        try {
            // create and open the connection
            conn = url.openConnection() as HttpsURLConnection

            // output = true, uploading POST data
            // input = true, downloading response to POST
            if (http == "POST")
                conn.doOutput = true

            conn.doInput = true
            // "GET" or "POST"
            conn.requestMethod = http

            // conn.setFixedLengthStreamingMode(params[1].getBytes().length);
            // send body unknown length
            // conn.setChunkedStreamingMode(0);
            conn.readTimeout = 10000
            conn.connectTimeout = 15000

            // set length of POST data to send then send it
            if (http == "POST") {
                conn.setRequestProperty("Content-Type",
                    "application/json; charset=UTF-8")
                conn.addRequestProperty("Content-Length", bytes.size.toString())

                Log.d(TAG, "post "+bytes.size.toString())
                Log.d(TAG, "post "+bytes)

                //send the POST out
                out = BufferedOutputStream(conn.outputStream)

                out.write(bytes)
                out.flush()
                out.close()
            } else {
                conn.addRequestProperty("Authorization", "Bearer " + params[3])
                conn.connect()
            }

            // logCertsInfo(conn);

            // now get response
            response = conn.responseCode

            /*
			 *  check the status code HTTP_OK = 200 anything else we didn't get what
			 *  we want in the data.
			 */
            if (response != HttpURLConnection.HTTP_OK) {
                Log.d(TAG, "Server returned: $response aborting read.")
                //Log.d(TAG, "Server json: "+ JSONObject(readIt(conn.inputStream)).toString())
                return getErrorJSON(R.string.co2_error)
            }
            instream = conn.inputStream
            contentAsString = readIt(instream)
            Log.d(TAG,contentAsString)
            return JSONObject(contentAsString)

        } finally {

            // Make sure that the Reader is closed after the app is finished using it.
            if (instream != null)
                try {
                    instream.close()
                } catch (ignore: IOException) {
                    Log.i(TAG, "instream IOException on close, ignored")
                }

            //* Make sure the connection is closed after the app is finished using it.
            if (conn != null)
                try {

                    conn.disconnect()
                } catch (ignore: IllegalStateException) {
                    Log.i(TAG, "socket IllegalStateException on disconnect, ignored")
                }

        }
    }

    /**
     * used to read input stream from http call.
     * @param stream InputStream
     * @return String
     */
    @Throws(IOException::class)
    fun readIt(stream: InputStream?): String {
        var bytesRead: Int
        var totalRead = 0
        val buffer = ByteArray(NETIOBUFFER)

        // for data from the server
        val bufferedInStream = BufferedInputStream(stream!!)
        // to collect data in our output stream
        val byteArrayOutputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(byteArrayOutputStream)

        // read the stream until end
        bytesRead = 0
        while (bytesRead != -1) {
            bytesRead = bufferedInStream.read(buffer)
            if (bytesRead > 0) {
                writer.write(buffer)
                totalRead += bytesRead
            }
        }
        writer.flush()
        Log.d(TAG, "Bytes read: " + totalRead
                + "(-1 means end of reader so max of)")
        Log.d(TAG, byteArrayOutputStream.toString())
        return byteArrayOutputStream.toString()
    }

    /**
     * Used to simply take a string from resources and put it inside of a JSONObject
     */
    private fun getErrorJSON(error_text_resource : Int) : JSONObject{
        return JSONObject("{\"error\":\""+getText(error_text_resource)+"\"}")
    }

    companion object {
        /**
         * Create a new instance of Co2CalculatorFragment, providing "num" as an
         * argument.
         */
        fun newInstance(): Co2CalculatorFragment {
            val f = Co2CalculatorFragment()
            return f
        }

        private val LOGINURL: String = "https://sustaincadinthomas.herokuapp.com/api/auth/login"
        private val TRIPINFOURL: String = "https://sustaincadinthomas.herokuapp.com/api/v1/tripinfo"
        private val AUTHENTICATIONJSON: String = "{ \"email\": \"cadinsl@gmail.com\" , \"password\":\"testtest\"}"
        private val NETIOBUFFER = 1024
        private val EMAIL: String = "j@j"
        private val PASSWORD: String = "1qazxsw2"
        private val FIXEDGASOLINEENGINE:String = "gasoline"
        private val FIXEDDIESELENGINE:String = "diesel"
        private val FIXEDCONSUMPTION:String = "10"
        private val TREECO2ABSORPTIONINKG: Double = 21.7724
        private val MY_PERMISSIONS_REQUEST_GET_LOCATION: Int = 1
        private val TAG:String = "Co2CalculatorFragment"
    }
}

package com.philip.sustainalexcadinphilipthomasvadim
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "trip_table")
data class Trip (
    @ColumnInfo(name = "tripName") var tripName : String,
    @ColumnInfo(name = "travelMode") var travelMode : String,
    @ColumnInfo(name = "fromLatitude") var fromLatitude : String,
    @ColumnInfo(name = "fromLongitude") var fromLongitude : String,
    @ColumnInfo(name = "toLatitude") var toLatitude : String,
    @ColumnInfo(name = "toLongitude") var toLongitude : String,
    @ColumnInfo(name = "distance") var distance : Double,
    @ColumnInfo(name = "co2") var co2 : Double,
    @ColumnInfo(name = "date") var date : String,
    @ColumnInfo(name = "time") var time : String,
    @ColumnInfo(name = "reason") var reason : String
)
{
    @ColumnInfo(name="id")
    @PrimaryKey  (autoGenerate = true)
    var id : Long =0
}
package com.philip.sustainalexcadinphilipthomasvadim

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(Trip::class), version = 1, exportSchema = false)
public abstract class TripRoomDatabase : RoomDatabase() {

    abstract fun tripDao(): TripDao

    private class TripDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch {
                    var tripDao = database.tripDao()

                    // Delete all content here.
                    //tripDao.deleteAll()

                    // Add sample trip

                    //var trip = Trip("test","car","40","50" +
                            //"60","70","80",90.0,100.0,"11/26/2019","10:32","testing")
                    //tripDao.insert(trip)
                    //trip = trip()
                    //tripDao.insert(trip)


                }
            }
        }
    }

    companion object{

        @Volatile
        private var INSTANCE: TripRoomDatabase? = null

        fun getDatabase(context : Context, scope: CoroutineScope): TripRoomDatabase{


            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    TripRoomDatabase::class.java,
                    "trip_database"
                )
                    .addCallback(TripDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}
package com.philip.sustainalexcadinphilipthomasvadim

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface TripDao {

    @Query("Select * from trip_table")
    fun getAllTrips(): LiveData<List<Trip>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(trip : Trip)


}
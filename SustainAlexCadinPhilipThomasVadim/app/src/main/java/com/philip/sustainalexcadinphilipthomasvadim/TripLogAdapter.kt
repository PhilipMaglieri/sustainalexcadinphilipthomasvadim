package com.philip.sustainalexcadinphilipthomasvadim

import android.annotation.SuppressLint
import android.content.Context
import android.text.TextUtils.substring
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.triprecyclerview_item.view.*

class TripLogAdapter internal constructor(
    context : Context
): RecyclerView.Adapter<TripLogAdapter.TripViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var trips = emptyList<Trip>()

    inner class TripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //val tripItemView: TextView = itemView.findViewById(R.id.)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripViewHolder {
        val itemView = inflater.inflate(R.layout.triprecyclerview_item, parent, false)
        return TripViewHolder(itemView)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TripViewHolder, position: Int) {
        val current = trips[position]

        holder.itemView.timeTextView.text = holder.itemView.timeTextView.text.toString().substringBefore(":") +": "+ current.time
        holder.itemView.distanceTextView.text = holder.itemView.distanceTextView.text.toString().substringBefore(":") +": "+ current.distance.toString()+"km"
        holder.itemView.tripNameTextView.text = holder.itemView.tripNameTextView.text.toString().substringBefore(":") +": "+ current.tripName
        holder.itemView.co2TextView.text = holder.itemView.co2TextView.text.toString().substringBefore(":") +": "+ String.format("%.4f",current.co2) +"kg"
        holder.itemView.dateTextView.text = holder.itemView.dateTextView.text.toString().substringBefore(":") +": "+ current.date
        holder.itemView.modeTextview.text = holder.itemView.modeTextview.text.toString().substringBefore(":") +": "+ current.travelMode
        holder.itemView.reasonTextView.text = holder.itemView.reasonTextView.text.toString().substringBefore(":") + ": "  + current.reason
        //holder.itemView.reasonTextView.text = holder.itemView.reasonTextView.text.toString() +": "+ current.reason
    }

    internal fun setTrips(trips: List<Trip>) {
        this.trips = trips
        notifyDataSetChanged()
    }

    override fun getItemCount() = trips.size
}
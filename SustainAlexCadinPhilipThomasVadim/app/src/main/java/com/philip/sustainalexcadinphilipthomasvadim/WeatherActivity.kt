package com.philip.sustainalexcadinphilipthomasvadim

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_weather.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

/**
 * Activity used for talking to the Open Weather API.
 * Based on the weather, informs user on the best way to get around given the conditions.
 *
 * @author Thomas Ouellette
 */
class WeatherActivity : AppCompatActivity() {

    private var uvi: String? = null
    private var weatherVerdict: String? = null
    private var temperature: String? = null
    private var weatherImage: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)

        uvi = savedInstanceState?.getString("UVI")
        weatherVerdict = savedInstanceState?.getString("Weather_Verdict")
        temperature = savedInstanceState?.getString("temperature")
        weatherImage = savedInstanceState?.getInt("image")
        Log.d(TAG, "Saved UV Index = $uvi")
        Log.d(TAG, "Saved weather verdict = $weatherVerdict")
        Log.d(TAG, "Saved temperature = $temperature")
        Log.d(TAG, "Saved image = $weatherImage")

        /*
            If any value isn't saved in the bundle,
            it assumes the activity has been opened for the first time.
         */
        if(uvi == null || weatherVerdict == null || temperature == null){

            val hasLocationPermission = ContextCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED

            if(hasLocationPermission){
                getWeatherUsingLocation()
            } else{
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), MY_PERMISSION_ACCESS_COURSE_LOCATION)
            }


        } else{
            Log.d(TAG, "Using values from saved bundle")
            weather_verdict.text = weatherVerdict
            weather_uvi.text = uvi
            weather_temperature.text = "${weather_temperature.text} ${Math.round((temperature)!!.toDouble())}°C"
            weather_image.setImageDrawable(ContextCompat.getDrawable(applicationContext,weatherImage!!))
        }

    } // onCreate

    /**
     * Save the weather verdict and UV index into the bundle
     * so that the web APIs aren't queried every time the
     * lifecycle methods are called (i.e. the screen rotates)
     */
    override fun onSaveInstanceState(outState: Bundle) {

        outState.run {
            putString("UVI", uvi)
            putString("Weather_Verdict", weatherVerdict)
            putString("temperature", temperature)
            putInt("image", weatherImage!!)
        }

        super.onSaveInstanceState(outState)
    }

    /**
     * This activity requires access to the device location. If the user grants it, the
     * Open Weather API will be queried. Otherwise, an error is displayed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSION_ACCESS_COURSE_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getWeatherUsingLocation()
                } else {
                    displayError(getText(R.string.location_error).toString())
                }
            }
        }
    }


    /**
     * Attempts to retrieve device's location from framework. After getting the device's
     * last known location, to uses it to retrieve the weather from the Open Weather API
     */
    private fun getWeatherUsingLocation(){

        Log.d(TAG, "Getting device location")

        /*
            Callback function used to check for device location updates.
            Calls the Open Weather API when it gets the device location.
        */
        val locationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    Log.d(TAG, "Got location")
                    // Removes location error
                    displayError("")
                    GetWeatherFromAPI().execute(location)
                    GetUVIndexFromAPI().execute(location)
                }
            }
        }

        val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationClient.lastLocation.addOnSuccessListener { location : Location? ->
            if(location != null){
                Log.d(TAG, "Got location")
                GetWeatherFromAPI().execute(location)
                GetUVIndexFromAPI().execute(location)
            } else{
                Log.e(TAG, "Couldn't get location")
                displayError(getText(R.string.location_error).toString())
                // if location was null, a location update is requested from the framework.
                fusedLocationClient.requestLocationUpdates(
                    LocationRequest(),
                    locationCallback,
                    Looper.getMainLooper())
            }
        }
    }

    /**
     * Calls the Open Weather API, retrieves the current UV index
     * for the device's last known location. After JSON response is received,
     * it displays a UV Index level based on the chart at:
     *
     * https://www.canada.ca/en/environment-climate-change/services/weather-health/uv-index-sun-safety.html
     *
     */
    private inner class GetUVIndexFromAPI : AsyncTask<Location, Void, JSONObject>() {

        override fun onPreExecute() {
            weather_uvi.text = getText(R.string.uvi_loading)
            Log.d(TAG, getText(R.string.uvi_loading).toString())
        }

        override fun doInBackground(vararg args: Location?): JSONObject {
            Log.d(TAG, "GetUVIndexFromAPI.doInBackground")
            val latitude = args[0]?.latitude
            val longitude = args[0]?.longitude
            return getJSONFromAPI("http://api.openweathermap.org/data/2.5/uvi?lat=$latitude&lon=$longitude&appid=2e5026c99504c28f1abe8cebede8f022")
        }

        override fun onPostExecute(result: JSONObject) {
            if(!result.has("error")) {
                val uvIndex = result["value"].toString().toDouble()
                Log.d(TAG, "UV Index = $uvIndex")

                uvi = when {
                    (uvIndex < 3.0) -> getText(R.string.uvi_low)
                    (uvIndex < 6.0) -> getText(R.string.uvi_moderate)
                    (uvIndex < 8.0) -> getText(R.string.uvi_high)
                    (uvIndex < 11.0) -> getText(R.string.uvi_very_high)
                    else -> getText(R.string.uvi_extreme)
                }.toString()

                weather_uvi.text = uvi
                Log.d(TAG, "UVI = $uvi")

            } else{
                weather_uvi.text = ""
                displayError(result["error"].toString())
            }
        }

    }

    /**
     * Calls the Open Weather API, retrieves the current weather for the device's
     * last known location. Based on the weather, it displays a verdict, i.e. whether the
     * user should use public transit, walk, or bike.
     */
    private inner class GetWeatherFromAPI : AsyncTask<Location, Void, JSONObject>() {

        override fun onPreExecute() {
            weather_verdict.text = getText(R.string.weather_loading)
            weather_temperature.visibility = View.GONE
            Log.d(TAG, getText(R.string.weather_loading).toString())
        }

        override fun doInBackground(vararg args: Location?): JSONObject {
            Log.d(TAG, "GetWeatherFromAPI.doInBackground")
            val latitude = args[0]?.latitude
            val longitude = args[0]?.longitude
            return getJSONFromAPI("http://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longitude&units=metric&APPID=2e5026c99504c28f1abe8cebede8f022")
        }

        override fun onPostExecute(result: JSONObject) {
            if(!result.has("error")) {
                Log.d(TAG, "$result")
                val currentTemp = (result["main"] as JSONObject)["temp"].toString().toDouble()
                temperature = currentTemp.toString()
                weather_temperature.visibility = View.VISIBLE
                weather_temperature.text = "${weather_temperature.text} ${Math.round(currentTemp)}°C"
                Log.d(TAG, "Current Temperature = $currentTemp")
                val currentWeather =
                    (((result["weather"] as JSONArray)[0]) as JSONObject).get("main").toString()
                        .toLowerCase()
                Log.d(TAG, "Current weather = $currentWeather")

                if (currentWeather.compareTo("rain") == 0 || currentWeather.compareTo("snow") == 0 || currentTemp < -5) {
                    weatherVerdict = getText(R.string.weather_bad).toString()
                    if(currentWeather.compareTo("rain") == 0){
                        weatherImage = R.drawable.rain
                    } else {
                        weatherImage = R.drawable.snow
                    }
                } else if (currentTemp < 15) {
                    weatherImage = R.drawable.sun
                    weatherVerdict = getText(R.string.weather_ok).toString()
                } else {
                    weatherImage = R.drawable.sun
                    weatherVerdict = getText(R.string.weather_good).toString()
                }

                weather_image.setImageDrawable(ContextCompat.getDrawable(applicationContext,weatherImage!!))
                weather_verdict.text = weatherVerdict
                Log.d(TAG, "Weather verdict = $weatherVerdict")
                // Make sure this is visible (may have been hidden if there had previously been an error)
                weather_activity_title.visibility = View.VISIBLE

            } else{
                weather_verdict.text = ""
                displayError(result["error"].toString())
            }
        }

    }


    /**
     * Takes a URL as a String, validates it, then sends a GET request to that url and retrieves
     * a JSON response. If an error occurs (IOException, HTTP error) it returns a JSONObject
     * containing a single key "error" with it's value being an error message.
     */
    private fun getJSONFromAPI(url: String): JSONObject {

        lateinit var urlObj: URL

        try{
            urlObj = URL(url)
        } catch(murle: MalformedURLException){
            return getErrorJSON(R.string.weather_error)
        }

        Log.d(TAG, "getJSONFromAPI $url")

        // Note: this way of checking for a connection is deprecated as of API 29
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return if (networkInfo != null && networkInfo.isConnected) {
            downloadFromURL(urlObj)
        } else {
            getErrorJSON(R.string.weather_network_error)
        }
    } // getJSONFromAPI()

    /**
     * Gets JSON response from a web API at the given url using a GET request.
     * If an error occurs (IOException, HTTP error) it returns a JSONObject
     * containing a single key "error" with it's value being an error message.
     */
    private fun downloadFromURL(url: URL): JSONObject{

        // Variables declared outside try for use in finally block
        var istream: InputStream? = null
        var conn: HttpURLConnection? = null

        try {
            conn = url.openConnection() as HttpURLConnection

            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = "GET"
            conn.doInput = true
            conn.connect()

            val response = conn.responseCode
            Log.d(TAG, "Server returned: $response")

            // if the response code isn't 200
            if (response != HttpURLConnection.HTTP_OK)
                return getErrorJSON(R.string.weather_error)

            // get the stream for the data from the website
            istream = conn.inputStream
            val reader = BufferedReader(InputStreamReader(istream!!))
            val jsonStringBuilder = StringBuilder()
            var line = reader.readLine()
            while(line != null){
                jsonStringBuilder.appendln(line)
                line = reader.readLine()
            }

            return JSONObject(jsonStringBuilder.toString())

        } catch (e: IOException) {
            Log.e(TAG, "IO exception in bg")
            e.printStackTrace()
            Log.getStackTraceString(e)
            return getErrorJSON(R.string.weather_error)
        } finally {
            if (istream != null) {
                try {
                    istream.close()
                } catch (ignore: IOException) {}

                if (conn != null)
                    try {
                        conn.disconnect()
                    } catch (ignore: IllegalStateException) {}
            }
        }
    }

    /**
     * Used to simply take a string from resources and put it inside of a JSONObject
     */
    private fun getErrorJSON(error_text_resource : Int) : JSONObject{
        return JSONObject("{\"error\":\""+getText(error_text_resource)+"\"}")
    }

    /**
     * Displays errors to the top of the UI.
     */
    private fun displayError(error: String){
        weather_error.text = error
        weather_error.visibility = View.VISIBLE
        weather_activity_title.visibility = View.GONE
    }

    companion object{
        val TAG = "WeatherActivity"
        const val MY_PERMISSION_ACCESS_COURSE_LOCATION = 1
    }
}

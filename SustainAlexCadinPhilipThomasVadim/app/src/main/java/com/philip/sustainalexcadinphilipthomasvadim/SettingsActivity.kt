package com.philip.sustainalexcadinphilipthomasvadim

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*

class SettingsActivity : AppCompatActivity() {
    private val TAG : String = "SettingsActivity"

    private lateinit var fname : EditText
    private lateinit var lname : EditText
    private lateinit var email : EditText
    private lateinit var password : EditText
    private lateinit var homeAddress : EditText
    private lateinit var homeStreet : EditText
    private lateinit var homeCity : EditText
    private lateinit var schoolAddress : EditText
    private lateinit var schoolStreet : EditText
    private lateinit var schoolCity : EditText
    private lateinit var sharedPref : SharedPreferences
    private var buttonPressed : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setTitle(getString(R.string.settings))
        sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)

        fname = findViewById(R.id.firstname)
        lname = findViewById(R.id.lastname)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        homeAddress = findViewById(R.id.homeAddress)
        homeStreet = findViewById(R.id.homeStreet)
        homeCity = findViewById(R.id.homeCity)
        schoolAddress = findViewById(R.id.schoolAddress)
        schoolStreet = findViewById(R.id.schoolStreet)
        schoolCity = findViewById(R.id.schoolCity)

        loadFromSharedPrefs()

        var fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            saveInfo()

        }
    }

    /**
     * This method checks if the input fields are filled out when the user clicks the fab,
     * if it is it will save the values into shared prefrences, if not it will
     * have a toast pop up saying it cannot save due to fields not being set
     */
    private fun saveInfo(){
        if(fname.length() == 0 || lname.length() == 0 || email.length() == 0 || password.length() == 0 ||
            homeStreet.length() == 0 || homeAddress.length() == 0 || schoolAddress.length() == 0 || homeCity.length() == 0 ||
            schoolStreet.length() == 0 || schoolCity.length() == 0  ){

            Toast.makeText(this,getString(R.string.cantSave),Toast.LENGTH_LONG).show()
            Log.i(TAG,"A field is not filled cannot save")
            //Make text into string literal
        }
        else{
            val dateFormater = SimpleDateFormat("dd/M/yyyy hh:mm")
            val date = dateFormater.format(Date())

            findViewById<TextView>(R.id.datestamp).text ="${getString(R.string.lastEdit)} $date"


            with(sharedPref.edit()){

                putString("firstname",fname.text.toString().trim())
                putString("lastname",lname.text.toString().trim())
                putString("email",email.text.toString().trim())
                putString("homeAddress",homeAddress.text.toString().trim())
                putString("homeStreet",homeStreet.text.toString().trim())
                putString("homeCity",homeCity.text.toString().trim())
                putString("schoolAddress",schoolAddress.text.toString().trim())
                putString("schoolStreet",schoolStreet.text.toString().trim())
                putString("schoolCity",schoolCity.text.toString().trim())
                putString("password",password.text.toString().trim())


                putString("dateModified",date.toString())
                commit()
                Log.i(TAG,"Saved Btn Clicked and info saved")
            }

            var usersHome = homeAddress.text.toString() + "|||" +homeStreet.text.toString() + "|||" +homeCity.text.toString() +"|||home"
            GetLocationFromAPI().execute(usersHome)

            var usersSchool = schoolAddress.text.toString() + "|||" +schoolStreet.text.toString() + "|||" +schoolCity.text.toString() +"|||school"
            GetLocationFromAPI().execute(usersSchool)

            Toast.makeText(this,getString(R.string.savedChanged),Toast.LENGTH_LONG).show()
            buttonPressed=true
        }
    }

    /**
     * This method will try to get a field from the shared prefrence and if its length
     * is larger than 1 it means all the shared prefrences are stored on the device
     * and we can proceed to set the edit text boxes with their previous enterd
     * values, minus their password,
     * (The SuppressLint is just supresses the warning when concatonating the date)
     */
    //@SuppressLint("SetTextI18n")
    private fun loadFromSharedPrefs(){
        val firstName : String? = sharedPref.getString("firstname","")

        if(firstName?.length != 0){
            Log.i(TAG,"Sharedpref info found")
            val lastName : String? = sharedPref.getString("lastname",null)
            val mail : String? = sharedPref.getString("email",null)
            val storedHomeAddress : String? = sharedPref.getString("homeAddress",null)
            val storedHomeStreet : String? = sharedPref.getString("homeStreet",null)
            val storedHomeCity : String? = sharedPref.getString("homeCity",null)
            val stroedSchoolAddress : String? = sharedPref.getString("schoolAddress",null)
            val stroedSchoolStreet : String? = sharedPref.getString("schoolStreet",null)
            val stroedSchoolCity : String? = sharedPref.getString("schoolCity",null)

            val lastEdit : String? = sharedPref.getString("dateModified","")


            fname.setText(firstName)
            lname.setText(lastName)
            email.setText(mail)
            homeAddress.setText(storedHomeAddress)
            homeCity.setText(storedHomeCity)
            homeStreet.setText(storedHomeStreet)
            schoolAddress.setText(stroedSchoolAddress)
            schoolCity.setText(stroedSchoolCity)
            schoolStreet.setText(stroedSchoolStreet)

            findViewById<TextView>(R.id.datestamp).text = getString(R.string.lastEdit) +" " +lastEdit
        }
    }

    /**
     * This method will check if the back button is pressed and if the users data is
     * saved before they exit. If its not saved it will prompt the user telling them
     * its not saved and they can decide wether they want to exit or if they want to
     * stay and finish their changes
     */
    override fun onBackPressed() {
        if(buttonPressed){
            super.onBackPressed()
        }
        else{
            val exitPrompt = AlertDialog.Builder(this)
            exitPrompt.setMessage(R.string.exitPrompt)
                .setPositiveButton(R.string.yesText) { _, _ ->
                   //Since the user is going to stay on the page, no action needs to be done
                }
                .setNegativeButton(R.string.noText) { _, _ ->
                    super.onBackPressed()
                }
            exitPrompt.create()
            exitPrompt.show()
        }
    }

    private inner class GetLocationFromAPI : AsyncTask<String, Void, JSONObject>() {
        private lateinit var location : String

        override fun onPreExecute() {
            super.onPreExecute()
        }

        override fun doInBackground(vararg args : String): JSONObject {
            Log.d(TAG, "GetLocationFromAPI.doInBackground")
            var localInfo = args[0].split("|||")

            var givenStreet = localInfo[1].replace(" ","%20")

            var number = localInfo[0]
            var street = givenStreet.trim()
            var city = localInfo[2]
            location = localInfo[3]

            return getJSONFromAPI("https://geocoder.api.here.com/6.2/geocode.json?housenumber=$number&street=$street&city=$city&country=canada&gen=9&app_id=rwr8A6UnZRS7AdcqC2Vr&app_code=3GzZO2oesN3TviosIlFDMg")
        }
        override fun onPostExecute(result: JSONObject) {
            var response = JSONObject(result.getJSONObject("Response").getJSONArray("View")[0].toString())
                var view = JSONObject(response.getJSONArray("Result")[0].toString())
                var locations = view.getJSONObject("Location")
                var navPos = JSONObject(locations.getJSONArray("NavigationPosition")[0].toString())
                if(location.equals("home")){
                    var sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)
                    Log.i(TAG,"Home " + navPos.get("Latitude").toString()+","+navPos.get("Longitude").toString())
                    with(sharedPref.edit()){
                        putString("home",navPos.get("Latitude").toString()+","+navPos.get("Longitude").toString())
                        commit()
                    }
                }
                else if (location.equals("school")){
                    var sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)
                    Log.i(TAG,"School " + navPos.get("Latitude").toString()+","+navPos.get("Longitude").toString())
                    with(sharedPref.edit()){
                        putString("school",navPos.get("Latitude").toString()+","+navPos.get("Longitude").toString())
                        commit()
                    }
                }


        }
    }

    /**
     * Takes a URL as a String, validates it, then sends a GET request to that url and retrieves
     * a JSON response. If an error occurs (IOException, HTTP error) it returns a JSONObject
     * containing a single key "error" with it's value being an error message. (From WeatherActivity By Thomas)
     */
    private fun getJSONFromAPI(url: String): JSONObject {

        lateinit var urlObj: URL

        try{
            urlObj = URL(url)
        }
        catch(murle: MalformedURLException){
            //CHECK ON CHANGING THIS
            //return getErrorJSON(R.string.weather_error)
            Log.i(TAG,"URL is invalid error caught in getJSONFromAPI")
        }

        Log.d(TAG, "getJSONFromAPI $url")

        // Note: this way of checking for a connection is deprecated as of API 29
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return if (networkInfo != null && networkInfo.isConnected) {
            downloadFromURL(urlObj)
        } else {
            Log.i(TAG,"Network Connection error getJSONFromAPI")
            getErrorJSON(R.string.weather_network_error)
        }
    }

    /**
     * Gets JSON response from a web API at the given url using a GET request.
     * If an error occurs (IOException, HTTP error) it returns a JSONObject
     * containing a single key "error" with it's value being an error message.
     * (From WeatherActivity By Thomas)
     */
    private fun downloadFromURL(url: URL): JSONObject{
        // Variables declared outside try for use in finally block
        var istream: InputStream? = null
        var conn: HttpURLConnection? = null

        try {
            conn = url.openConnection() as HttpURLConnection

            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = "GET"
            conn.doInput = true
            conn.connect()

            val response = conn.responseCode
            Log.d(TAG, "Server returned: $response")

            // if the response code isn't 200
            if (response != HttpURLConnection.HTTP_OK)
                return getErrorJSON(R.string.weather_error)

            // get the stream for the data from the website
            istream = conn.inputStream
            val reader = BufferedReader(InputStreamReader(istream!!))
            val jsonStringBuilder = StringBuilder()
            var line = reader.readLine()
            while(line != null){
                jsonStringBuilder.appendln(line)
                line = reader.readLine()
            }

            return JSONObject(jsonStringBuilder.toString())

        } catch (e: IOException) {
            Log.e(TAG, "IO exception in background")
            e.printStackTrace()
            Log.getStackTraceString(e)
            return getErrorJSON(R.string.weather_error)
        } finally {
            if (istream != null) {
                try {
                    istream.close()
                } catch (ignore: IOException) {}

                if (conn != null)
                    try {
                        conn.disconnect()
                    } catch (ignore: IllegalStateException) {}
            }
        }
    }
    /**
     * Used to simply take a string from resources and put it inside of a JSONObject
     * (From WeatherActivity By Thomas)
     */
    private fun getErrorJSON(error_text_resource : Int) : JSONObject{
        return JSONObject("{\"error\":\""+getText(error_text_resource)+"\"}")
    }

}

package com.philip.sustainalexcadinphilipthomasvadim

import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class QuickTripActivity : AppCompatActivity() {
    private val TAG : String = "QuickTripActivity"

    private lateinit var reason : EditText
    private lateinit var nameOfTrip : EditText
    private lateinit var sharedPref : SharedPreferences
    private lateinit var modeSpinner : Spinner
    private lateinit var saveToSpinner : Spinner
    private lateinit var carco2 : TextView
    private lateinit var busco2 : TextView

    private lateinit var modeSpinnerValues : Array<String>
    private var modeSelected : Boolean = false
    private lateinit var saveSpinnerValues : Array<String>
    private var saveModeSelected : Boolean = false

    private lateinit var saveMode : String
    private lateinit var travelMode : String

    private lateinit var destinationLat : String
    private lateinit var destinationLong : String

    private lateinit var fromLat : String
    private lateinit var fromLong : String

    private var saveClick : Boolean =  false
    private var saveTrip : Boolean = false
    private var co2Car : String = ""
    private var co2Bus : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quick_trip)

        sharedPref = getSharedPreferences("user_info", Context.MODE_PRIVATE)

        title = (intent.extras!!.getString("tripTo") as String)

        reason = findViewById(R.id.reasonForTrip)
        nameOfTrip = findViewById(R.id.nameOfTrip)

        modeSpinner = findViewById(R.id.transportationModeSpinner)
        transportationModeSpinnerSetUp()

        saveToSpinner = findViewById(R.id.saveToSpinner)
        savedSpinnerSetUp()

        carco2 =  findViewById(R.id.co2Car)
        busco2 = findViewById(R.id.co2Bus)

        getConnectionForAPICall()

        locationSetUp()

        var fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            saveInfo()
        }
        resettingOldState(savedInstanceState)
    }

    private fun resettingOldState(savedInstanceState: Bundle?){
        if (savedInstanceState != null) {
            nameOfTrip.setText(savedInstanceState.getString("nameOfTrip"))
            reason.setText(savedInstanceState.getString("reason"))
        }

    }

    private fun saveInfo(){
        if(reason.length() == 0 || nameOfTrip.length() == 0 || !modeSelected || !saveModeSelected){
            Toast.makeText(this,getString(R.string.cantSave), Toast.LENGTH_LONG).show()
        }
        else{
            if(saveMode.equals(getString(R.string.saveLocally))){
                saveClick=true;
                getConnectionForAPICall()
                Toast.makeText(this,getString(R.string.saved),Toast.LENGTH_LONG).show()
            }
            else{
                saveTrip = true
                Toast.makeText(this,"Information Saved cloud ", Toast.LENGTH_LONG).show()
                getConnectionForAPICall()
            }
            //Create a trip with the given information, get home
        }
    }

    /**
     * This method creates the options in the drop down presented to the user to
     * select their mode of transportation, the onclick checks if the
     * user selected something that isn't the default value
     */
    private fun transportationModeSpinnerSetUp(){
        //getString(R.string.travel_mode_car)
        modeSpinnerValues = arrayOf(getString(R.string.radio_travel_mode),getString(R.string.travel_mode_walk_bike), getString(R.string.travel_mode_car),getString(R.string.travel_mode_car_electric),getString(R.string.travel_mode_bus))
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, modeSpinnerValues)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        modeSpinner.adapter = arrayAdapter

        modeSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                modeSelected = !modeSpinnerValues[position].equals(getString(R.string.radio_travel_mode))
                travelMode = modeSpinnerValues[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>){}
        }
    }

    /**
     * This method creates the options in the drop down presented to the user to
     * select where they want to save this trip info, the onclick checks if the
     * user selected something that isn't the default value
     */
    private fun savedSpinnerSetUp(){
        saveSpinnerValues = arrayOf(getString(R.string.saveTo), getString(R.string.saveLocally), getString(R.string.saveCloud))
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, saveSpinnerValues)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        saveToSpinner.adapter = arrayAdapter

        saveToSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent:AdapterView<*>, view: View, position: Int, id: Long){
                saveModeSelected = !saveSpinnerValues[position].equals(getString(R.string.saveTo))
                saveMode = saveSpinnerValues[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>){}
        }
    }

    /**
     * This method appropriately sets instance variables to be used
     * when adding the quick trip, home or school, to the database
     */
    private fun locationSetUp(){
        val toLatAndLong : String?
        val toSplitCoordinates : List<String>?

        if(title == getString(R.string.tripToHome)){
            toLatAndLong = sharedPref.getString("home","notFound")
            toSplitCoordinates = toLatAndLong?.split(",")
        }
        else{
            toLatAndLong = sharedPref.getString("school","notFound")
            toSplitCoordinates = toLatAndLong?.split(",")
        }

        destinationLat = toSplitCoordinates?.get(0).toString()
        destinationLong = toSplitCoordinates?.get(1).toString()

        val locationPermission = ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED
        if(locationPermission){
            getCurrentLocation()
        }
        else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),WeatherActivity.MY_PERMISSION_ACCESS_COURSE_LOCATION)
        }
    }

    private fun getCurrentLocation(){
        val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        fusedLocationClient.lastLocation.addOnSuccessListener { location : Location? ->
            if(location != null){
                Log.i("TESTING", location?.latitude.toString() + "," +location?.longitude.toString())
                fromLat = location?.latitude.toString()
                fromLong = location?.longitude.toString()
            }
            else{
                Toast.makeText(this,getString(R.string.location_error),Toast.LENGTH_LONG).show()
            }
        }
    }


    fun getConnectionForAPICall(){
        Log.d(TripLogActivity.TAG,"getConnectionForAPICall")
        val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = manager.activeNetworkInfo
        val login = "https://jayaghgtracker.herokuapp.com/api/auth/login"
        val authentication : String = "{ \"email\": \"j@j\" , \"password\":\"1qazxsw2\"}"
        if(info != null && info.isConnected){
            GetTokenFromAPI().execute(login, authentication, "POST","")
        }
        else {
            Log.e(TripLogActivity.TAG, "internet conncection error")
        }
    }

    private inner class GetTokenFromAPI : AsyncTask<String, Void, JSONObject>() {

        override fun doInBackground(vararg args: String): JSONObject {
            Log.d(TripLogActivity.TAG, "getTokenFromAPI.doInBackground")

            try {
                return downloadFromURL(*args)
            }
            catch (e : IOException){
                return getErrorJSON(R.string.trip_load_error)
            }
        }

        override fun onPostExecute(result: JSONObject) {
            Log.d(TripLogActivity.TAG, "onPostExecute getTokenFromAPI ")
            val token = result["access_token"].toString()
            var url : String = ""

            if(saveTrip)
            {
                val url = "https://jayaghgtracker.herokuapp.com/api/v1/addtrip"
                val postDataJSON = JSONObject()
                postDataJSON.put("fromlatitude", fromLat)
                postDataJSON.put("fromlongitude", fromLong)
                postDataJSON.put("tolatitude", destinationLat)
                postDataJSON.put("tolongitude", destinationLong)
                if(travelMode == getString(R.string.travel_mode_walk_bike))
                {
                    postDataJSON.put("mode", "pedestrian")
                }
                else if(travelMode == getString(R.string.travel_mode_bus))
                {
                    postDataJSON.put("mode", "publicTransport")
                }
                else if(travelMode == getString(R.string.travel_mode_car))
                {
                    postDataJSON.put("mode", "car")
                    postDataJSON.put("engine", "gasoline")
                    postDataJSON.put("consumption", "10.1")
                }
                else if(travelMode == getString(R.string.travel_mode_car_electric) )
                {
                    postDataJSON.put("mode", "car")
                    postDataJSON.put("engine", "electric")
                    postDataJSON.put("consumption", "0")
                }
                saveTrip = false
                postAddTripFromAPI().execute(url,postDataJSON.toString(),"POST", token)
            }
            else {
                if (travelMode == getString(R.string.travel_mode_car)) {
                    url =
                        "?fromlatitude=$fromLat&fromlongitude=$fromLong&tolatitude=$destinationLat&tolongitude=$destinationLong&mode=car&engine=gasoline&consumption=10.1"
                } else if (travelMode == getString(R.string.travel_mode_walk_bike)) {
                    url =
                        "?fromlatitude=$fromLat&fromlongitude=$fromLong&tolatitude=$destinationLat&tolongitude=$destinationLong&mode=pedestrian"
                } else if (travelMode == getString(R.string.travel_mode_bus)) {
                    url =
                        "?fromlatitude=$fromLat&fromlongitude=$fromLong&tolatitude=$destinationLat&tolongitude=$destinationLong&mode=publicTransport"
                } else if (co2Car.equals("")) {
                    url =
                        "?fromlatitude=$fromLat&fromlongitude=$fromLong&tolatitude=$destinationLat&tolongitude=$destinationLong&mode=car&engine=gasoline&consumption=10.1"
                } else if (co2Bus.equals("")) {
                    url =
                        "?fromlatitude=$fromLat&fromlongitude=$fromLong&tolatitude=$destinationLat&tolongitude=$destinationLong&mode=publicTransport"
                }


                val infoAPI = "https://jayaghgtracker.herokuapp.com/api/v1/tripinfo"
                GetTripDetailsFromAPI().execute(infoAPI, url, "GET", token)
            }
        }

        /**
         * based on weather activity and CO2 frag perhaps transfer to a common class for
         * data retreival
         * small changes made to corrrespond to this activity
         */
        @Throws(IOException::class)
        private fun downloadFromURL(vararg args: String): JSONObject{
            Log.d(TripLogActivity.TAG,"downloadFromURL ()")
            var istream: InputStream? = null
            var outStream : OutputStream
            var conn: HttpURLConnection? = null
            var data : String
            val response : Int
            val url : URL
            val urlString : String

            var bytes : ByteArray = "".toByteArray(charset("UTF-8"))
            var method = args[2]
            if (method == "GET") {
                urlString = args[0] + args[1]
            } else {
                urlString = args[0]
                bytes = args[1].toByteArray(charset("UTF-8"))
            }
            Log.i(TAG, "URL "+urlString)

            try{
                url = URL(urlString)
            }
            catch(e: MalformedURLException){
                return getErrorJSON(R.string.trip_load_error)
            }

            try {
                conn = url.openConnection() as HttpURLConnection

                if(method == "POST"){
                    conn.doOutput = true
                }

                conn.readTimeout = 10000
                conn.connectTimeout = 15000
                conn.requestMethod = method
                conn.doInput = true

                if(method == "POST"){
                    conn.addRequestProperty("Authorization", "Bearer" + args[3])
                    conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                    conn.addRequestProperty("Content-Length", bytes.size.toString())

                    outStream = BufferedOutputStream(conn.outputStream)
                    outStream.write(bytes)
                    outStream.flush()
                    outStream.close()
                }
                else{
                    conn.addRequestProperty("Authorization", "Bearer" + args[3])
                    conn.connect()
                }

                response = conn.responseCode
                Log.d(TripLogActivity.TAG, "Server returned: $response")

                // if the response code isn't 200
                if (response != HttpURLConnection.HTTP_OK) {
                    return getErrorJSON(R.string.trip_load_error)
                }

                // get the stream for the data from the website
                istream = conn.inputStream
                data = readStream(istream)

                return JSONObject(data)

            } catch (e: IOException) {
                Log.e(TripLogActivity.TAG, "IO exception in bg")
                e.printStackTrace()
                Log.getStackTraceString(e)
                return getErrorJSON(R.string.trip_load_error)
            } finally {
                if (istream != null) {
                    try {
                        istream.close()
                    } catch (ignore: IOException) {}

                    if (conn != null)
                        try {
                            conn.disconnect()
                        } catch (ignore: IllegalStateException) {}
                }
            }
        }

        fun readStream(inStream : InputStream?): String{
            Log.d(TripLogActivity.TAG,"readStream ()")
            var read : Int
            var total = 0
            val buffer = ByteArray(1024)

            val bufferedStream = BufferedInputStream(inStream!!)
            val outputStream = ByteArrayOutputStream()
            val writer = DataOutputStream(outputStream)

            read = 0

            while(read != -1){
                read = bufferedStream.read(buffer)
                if(read>0){
                    writer.write(buffer)
                    total += read
                }
            }
            writer.flush()

            return outputStream.toString()
        }

        private fun getErrorJSON(error_text_resource : Int) : JSONObject{
            return JSONObject("{\"error\":\""+getText(error_text_resource)+"\"}")
        }

        private inner class GetTripDetailsFromAPI : AsyncTask<String,Void,JSONObject>(){
            override fun onPreExecute() {
                Log.i(TripLogActivity.TAG, "calling trip details API")

            }

            override fun doInBackground(vararg args: String): JSONObject {
                Log.d(TripLogActivity.TAG,"doInBackground GetTripDetailsFrom ApI")
                try{
                    return downloadFromURL(*args)
                }
                catch(e : IOException){
                    return getErrorJSON(R.string.trip_load_error)
                }
            }

            override fun onPostExecute(result: JSONObject){
                Log.d(TripLogActivity.TAG,"onPostExecute GetTripDetailsFrom ApI")
                if(result.has("error")){
                    Toast.makeText(
                        applicationContext,
                        R.string.trip_load_error,
                        Toast.LENGTH_LONG).show()
                    return
                }

                var co2 : Double

                var distance = result["distance"].toString().toDouble()
                var distanceInKms = distance/1000

                if(travelMode == getString(R.string.travel_mode_bus)){
                    co2 = (42.2/1000*distanceInKms)
                }
                else{
                    co2 = result["co2emissions"].toString().toDouble()
                }

                val dateFormater = SimpleDateFormat("dd/M/yyyy")
                val date = dateFormater.format(Date())

                var time = result["traveltime"].toString().toDouble()
                var timeInMinutes = (time/60)


                if(saveClick){
                    val trip = Trip( nameOfTrip.text.toString(), travelMode,fromLat,fromLong,destinationLat,destinationLong,
                        distanceInKms, co2, date, timeInMinutes.roundToInt().toString(), reason.text.toString())
                    val tripViewModel = ViewModelProvider(this@QuickTripActivity).get(TripViewModel::class.java)
                    tripViewModel.insert(trip)
                }
                else if(co2Car.equals("")){
                    co2Car = result["co2emissions"].toString()
                    carco2.text = carco2.text.toString() + "" + co2Car
                    getConnectionForAPICall() //This second call is to execute the if statement below
                }
                else if(co2Bus.equals("")){
                    var distance = result["distance"].toString().toDouble()
                    var distanceInKms = distance/1000
                    co2Bus = (42.2/1000*distanceInKms).toString()

                    busco2.text = busco2.text.toString() +""+ co2Bus.substring(0,5)
                }
            }
        }

        private inner class postAddTripFromAPI : AsyncTask<String,Void,JSONObject>(){
            override fun onPreExecute() {
                Log.i(TripLogActivity.TAG, "calling trip details API")

            }

            override fun doInBackground(vararg args: String): JSONObject {
                Log.d(TripLogActivity.TAG,"doInBackground add Trip to API")
                try{
                    return downloadFromURL(*args)
                }
                catch(e : IOException){
                    Log.d(TripLogActivity.TAG, "error adding trip")
                    return getErrorJSON(R.string.trip_load_error)
                }
            }

            override fun onPostExecute(result: JSONObject){
                Log.d(TripLogActivity.TAG,"onPostExecute add Trip to API")
                if(result.has("error")) {
                    Log.d(TripLogActivity.TAG, "error response from add trip")
                    Toast.makeText(
                        applicationContext,
                        R.string.trip_add_error,
                        Toast.LENGTH_LONG
                    ).show()
                    return
                }
                Log.d(TripLogActivity.TAG, "ADD TRIP $result")
                Toast.makeText(applicationContext, "success", Toast.LENGTH_LONG).show()
                return

            }
        }

    }

}


package com.philip.sustainalexcadinphilipthomasvadim

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        val alexPic : ImageView = findViewById(R.id.imageViewAb)
        val cadinPic : ImageView = findViewById(R.id.imageViewCl)
        val philPic : ImageView = findViewById(R.id.imageViewPm)
        val thomasPic : ImageView = findViewById(R.id.imageViewTo)
        val vladimPic : ImageView = findViewById(R.id.imageViewVk)

    }

    fun blurbAboutTeam(view: View) {
        val builder = AlertDialog.Builder(this@AboutActivity)
        builder.setTitle(R.string.blurb)
        builder.setMessage(R.string.blurbTeam)
        builder.setPositiveButton(R.string.ok){ dialog, which -> }

        builder.show()
        //Toast.makeText(this,"Blurb about the team",Toast.LENGTH_LONG).show()

    }

}

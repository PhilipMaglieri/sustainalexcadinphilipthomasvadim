package com.philip.sustainalexcadinphilipthomasvadim

import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.LocationServices
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class TripLogRemoteActivity : AppCompatActivity() {

    private var tripsList : ArrayList<Trip> = arrayListOf()
    private var tripName : String = ""
    private var travelMode : String = ""
    private var fromLatitude : String = ""
    private var fromLongitude : String = ""
    private var toLatitude : String = ""
    private var toLongitude : String = ""
    private var distance : Double = 0.0
    private var co2 : Double = 0.0
    private var date : String = ""
    private var time : String = ""
    private var reason : String = ""
    private var destination : String = ""
    private lateinit var adapter : TripLogAdapter
    private var loginRequest : Boolean = true
    private var numTrips : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.trip_log_view)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        adapter = TripLogAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener{
            Toast.makeText(
                applicationContext,
                R.string.trip_loading,
                Toast.LENGTH_LONG).show()
            getAllTrips()
        }


    }

     fun getAllTrips(){
        //val tripLogActivity : TripLogActivity = TripLogActivity()
        //getting error when trying to use fun from other class cannot get resources before oncreate...
        //tripLogActivity.getConnectionForAPICall()

        getConnectionForAPICall()

    }

    fun getConnectionForAPICall() {
            Log.d(TAG, "getConnectionForAPICall")
            val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val info = manager.activeNetworkInfo
            val login = "https://sustaincadinthomas.herokuapp.com/api/auth/login"

            val sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)
            val username = sharedPref.getString("email",null)
            val password = sharedPref.getString("password",null)

            val authentication: String = "{ \"email\": \"$username\" , \"password\":\"$password\"}"
            if (info != null && info.isConnected) {
                GetTokenFromAPI().execute(login, authentication, "POST", "")
            } else {
                Log.e(TAG, "internet conncection error")
            }
    }

    private inner class GetTokenFromAPI: AsyncTask<String, Void, JSONObject>(){
        override fun onPreExecute() {
            Log.d(TAG, "on preExecute() getTokenFromAPI")
        }

        override fun doInBackground(vararg args: String): JSONObject {
            try {
                return downloadFromURL(*args)
            }
            catch (e : IOException){
                return getErrorJSON(R.string.trip_load_error)
            }
        }

        override fun onPostExecute(result: JSONObject){
            Log.d(TAG, "onPostExecute getTokenFromAPI ")
            println(result.toString())
            loginRequest = false
            val token = result["access_token"].toString()
            var url : String = "https://sustaincadinthomas.herokuapp.com/api/v1/alltrips"
            GetAllTripsDetailsFromAPI().execute("",url,"GET",token)
        }
    }

    private inner class GetAllTripsDetailsFromAPI : AsyncTask<String,Void,JSONObject>(){
        override fun onPreExecute() {
            Log.i(TAG, "calling trip details API")
        }

        override fun doInBackground(vararg args: String): JSONObject {
            Log.d(TAG,"doInBackground GetTripDetailsFrom ApI")
            try{
                return downloadFromURL(*args)
            }
            catch(e : IOException){
                return getErrorJSON(R.string.trip_load_error)
            }
        }

        override fun onPostExecute(result: JSONObject){
            Log.d(TAG,"onPostExecute GetTripDetailsFrom ApI")
            if(result.has("error")){
                Toast.makeText(
                    applicationContext,
                    R.string.trip_load_error,
                    Toast.LENGTH_LONG).show()
                return
            }

            println(result.toString())

            //need for loop here for each trip to add to list
            for(i in 0..numTrips -1 ) {
                tripName = "N/A"
                travelMode = result.getJSONObject(i.toString()).getString("transportationMode")
                fromLatitude = result.getJSONObject(i.toString()).getString("startLocationId")
                fromLongitude = result.getJSONObject(i.toString()).getString("startLocationId")
                toLatitude = result.getJSONObject(i.toString()).getString("destinationLocationId")
                toLongitude = result.getJSONObject(i.toString()).getString("destinationLocationId")
                date = result.getJSONObject(i.toString()).getString("created_at")
                co2 = result.getJSONObject(i.toString()).getDouble("co2emissions")
                distance = result.getJSONObject(i.toString()).getDouble("distance")
                var distanceInKms = distance / 1000
                time = result.getJSONObject(i.toString()).getString("time")
                var timeInMinutes = time.toDouble()
                val trip = Trip(
                    tripName, travelMode, fromLatitude, fromLongitude, toLatitude, toLongitude,
                    distanceInKms, co2, date, timeInMinutes.roundToInt().toString(), reason
                )
                tripsList.add(trip)
            }


            adapter.setTrips(tripsList)

        }
    }

    /**
     * based on weather activity and CO2 frag perhaps transfer to a common class for
     * data retreival
     * small changes made to corrrespond to this activity
     */
    @Throws(IOException::class)
    fun downloadFromURL(vararg args: String) : JSONObject{
        Log.d(TAG, "downloadFromURL()")
        var istream: InputStream? = null
        var outStream : OutputStream
        var conn: HttpURLConnection? = null
        var data : String
        val response : Int
        val url : URL
        val urlString : String

        var bytes : ByteArray = "".toByteArray(charset("UTF-8"))
        var method = args[2]
        if (method == "GET") {
            urlString = args[0] + args[1]
        } else {
            urlString = args[0]
            bytes = args[1].toByteArray(charset("UTF-8"))
        }

        try{
            url = URL(urlString)
        }
        catch(e: MalformedURLException){
            return getErrorJSON(R.string.trip_load_error)
        }

        try {
            conn = url.openConnection() as HttpURLConnection

            if(method == "POST"){
                conn.doOutput = true
            }

            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = method
            conn.doInput = true

            if(method == "POST"){
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                conn.addRequestProperty("Content-Length", bytes.size.toString())

                outStream = BufferedOutputStream(conn.outputStream)
                outStream.write(bytes)
                outStream.flush()
                outStream.close()
            }
            else{
                conn.addRequestProperty("Authorization", "Bearer" + args[3])
                conn.connect()
            }



            response = conn.responseCode
            Log.d(TripLogActivity.TAG, "Server returned: $response")

            // if the response code isn't 200
            if (response != HttpURLConnection.HTTP_OK) {
                return getErrorJSON(R.string.trip_load_error)
            }

            // get the stream for the data from the website
            istream = conn.inputStream
            data = readStream(istream)

            var jsonArray = JSONArray()
            // Whatever works, right? ;)
            if (loginRequest){
                return JSONObject(data)
            }

            else{
                var array = JSONArray(data)
                numTrips = array.length()
                for(i in 0.. array.length()-1){

                    var trip =array.get(i) as JSONObject

                    jsonArray.put(i)
                }

                return array.toJSONObject(jsonArray)
            }
            /**try{

                return array.toJSONObject(array)
            } catch(e: JSONException){

            }**/



        } catch (e: IOException) {
            Log.e(TAG, "IO exception in bg")
            e.printStackTrace()
            Log.getStackTraceString(e)
            return getErrorJSON(R.string.trip_load_error)
        } finally {
            if (istream != null) {
                try {
                    istream.close()
                } catch (ignore: IOException) {}

                if (conn != null)
                    try {
                        conn.disconnect()
                    } catch (ignore: IllegalStateException) {}
            }
        }
    }

    /**
     * inspired by co2calculator class same call is made to get trip info
     * reads bytes
     */
    fun readStream(inStream : InputStream?): String{
        Log.d(TAG,"readStream ()")
        var read : Int
        var total = 0
        val buffer = ByteArray(1024)

        val bufferedStream = BufferedInputStream(inStream!!)
        val outputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(outputStream)

        read = 0

        while(read != -1){
            read = bufferedStream.read(buffer)
            if(read>0){
                writer.write(buffer)
                total += read
            }
        }
        writer.flush()

        return outputStream.toString()
    }


    /**
     * taken from weather activity perhaps transfer to a common class for
     * data retrieval
     *
     */
    private fun getErrorJSON(error_text_resource : Int) : JSONObject{
        return JSONObject("{\"error\":\""+getText(error_text_resource)+"}")
    }


    companion object{
        val TAG = "TripLog Remote Activity"
    }
}

package com.philip.sustainalexcadinphilipthomasvadim

import android.content.Intent
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FileDownloadTask
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_facts.*
import java.io.File
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.ValueEventListener
import android.net.Uri
import kotlin.random.Random
import android.util.Log
import android.content.ActivityNotFoundException
import android.view.GestureDetector
import android.view.GestureDetector.OnGestureListener
import android.view.MotionEvent
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.MotionEventCompat
import kotlin.math.abs

/**
 * Loading random facts from firebase realtime database
 *
 */


class FactsActivity : AppCompatActivity(),
    OnGestureListener {

    private val TAG: String = "Facts-Act"

    lateinit var storageRef: StorageReference
    lateinit var factsRef: DatabaseReference
    private var fact: Fact = Fact()
    private lateinit var mDetector: GestureDetectorCompat

    private val VELOCITY_IMPORTANCE : Float = 0.06f;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facts)

        val database = FirebaseDatabase.getInstance()
        factsRef = database.getReference("facts")
        storageRef = FirebaseStorage.getInstance().reference
        if (savedInstanceState != null) {
            with(savedInstanceState) {
                fact = Fact(
                    getString("text") ?: "something went wrong",
                    getString("uri") ?: "",
                    getString("imageName") ?: ""
                )
            }
            displayFact()
            Log.d(TAG, "State info restored successfully")
        }else{
            newFact()
        }

        mDetector = GestureDetectorCompat(this, this)
        Log.d(TAG, "onCreate")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState?.run {
            // saving a fact
            putString("uri", fact.url)
            putString("text", fact.text)
            putString("imageName", fact.imageName)
        }
        super.onSaveInstanceState(outState)
        Log.d(TAG, "State info saved temporarily")
    }

    fun goToSource(view: View) {
        try {
            if (fact.url != "") {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(fact.url)))
            }
        } catch (ex: ActivityNotFoundException) {
            Log.e(TAG, "The url is wrong $fact.url")
        }
    }

    fun newFact(view: View) {
        newFact()
        Log.d(TAG, "newFact handler was called")
    }

    private fun newFact() {
        factsRef.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val facts : List<DataSnapshot> = dataSnapshot.children.toList()
                val factNumber: Int = Random.nextInt(0, facts.size)
                val value = facts[factNumber].getValue(Fact::class.java)
                fact = Fact(value?.text ?: "", value?.url ?: "", value?.imageName ?: "")
                displayFact()
                Log.d(TAG, "new fact was loaded and selected from database")
            }

            override fun onCancelled(databaseError: DatabaseError) {
                fact = Fact("something went wrong","https://www.dawsoncollege.qc.ca/sustainable/","")
                displayFact()
                Log.d(TAG, "An error happened while getting data from firebase")
            }
        })
    }

    private fun displayFact() {
        factText.text = fact.text
        if (fact.imageName != "") {
            val imageRef = storageRef.child(fact.imageName)
            val localFile = File.createTempFile("images", "jpg")
            imageRef.getFile(localFile)
                .addOnSuccessListener(OnSuccessListener<FileDownloadTask.TaskSnapshot> {
                    val myBitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                    factImage.setImageBitmap(myBitmap)
                }).addOnFailureListener(OnFailureListener {
                })
        }
        Log.d(TAG, "fact was displayed")
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (mDetector.onTouchEvent(event)) {
            true
        } else {
            super.onTouchEvent(event)
        }
    }

    override fun onShowPress(e: MotionEvent?) { }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {return false}

    override fun onDown(e: MotionEvent?): Boolean {return false}

    override fun onFling(
        downEvent : MotionEvent?,
        upEvent: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        Log.d(TAG, "onFling: $downEvent $upEvent $velocityX $velocityY")
        val xDiff : Float = abs(downEvent?.x?.minus((upEvent?.x ?: 0f)) ?:  0f)
        val yDiff : Float = abs(downEvent?.x?.minus((upEvent?.x ?: 0f)) ?:  0f)

        val actionConfidence : Float = xDiff + yDiff + (abs(velocityX) + abs(velocityY)) * VELOCITY_IMPORTANCE
        if(actionConfidence > 290f){
            newFact()
            return true
        }
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) { }
}

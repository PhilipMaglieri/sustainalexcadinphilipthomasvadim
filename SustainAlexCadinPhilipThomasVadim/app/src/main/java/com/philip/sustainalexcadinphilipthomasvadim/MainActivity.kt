package com.philip.sustainalexcadinphilipthomasvadim


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import java.lang.Math.ceil


class MainActivity : AppCompatActivity() {
    private val TAG : String = "MainActivity"
    private lateinit var sharedPref : SharedPreferences
    private lateinit var tripLogButton : ImageButton
    private lateinit var calculator : ImageButton
    private lateinit var totalCO2ToDate : TextView

    private var co2Total : Double = 0.0
    private val fragmentManager = supportFragmentManager
    private val fragmentTransaction = fragmentManager.beginTransaction()
    private val tripActivityRequestCode = 1;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTitle(R.string.Sustain)
        setContentView(R.layout.activity_main)

        totalCO2ToDate = findViewById(R.id.totalemissions)

        sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)

        if(!sharedPref.contains("firstname")){
            Log.i(TAG,"Redirecting to settings, Settings not set")
            Toast.makeText(this,R.string.noSettings,Toast.LENGTH_LONG).show()
            startActivity(Intent(Intent(this, SettingsActivity::class.java)))
        }
        else {
            Log.i(TAG,"User Info Found")
        }

        if(!sharedPref.contains("co2")){
            Log.i(TAG,"no co2 found")
        }
        else {
            Log.i(TAG, "Found CO2")
            co2Total = sharedPref.getFloat("co2",0F).toDouble()
            var trees = co2Total/21.7724
            totalCO2ToDate.text = getString(R.string.totalCO2ToDate) + " " + String.format("%.2f",co2Total) + " " + getString(R.string.co2_calculation_response_2)+" "+ String.format("%.2f",trees) + " " + getString(R.string.trees)
        }

        /**setTitle(R.string.Sustain)
        setContentView(R.layout.activity_main)**/

        /*
        val firstFragment: Co2CalculatorFragment = Co2CalculatorFragment()
        val ft = supportFragmentManager.beginTransaction()// begin  FragmentTransaction


        if (savedInstanceState == null) {
            // Instance of first fragment
            // Add Fragment to FrameLayout (flContainer), using FragmentManager
            ft.add(R.id.flContainer, firstFragment)                                // add    Fragment
            ft.commit()                                                            // commit FragmentTransaction
        } else {
            // if this is not done, the menu fragment ended up duplicate
            //  on rotate back to landscape, only when the menu activity was
            //  last active
            ft.replace(R.id.flContainer, firstFragment)                                // add    Fragment
            ft.commit()                                                            // commit FragmentTransaction
        }
         */
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.drop_down_menu,menu)
        Log.i(TAG,"Option Menu created")
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        when(item.itemId){
            R.id.aboutpage -> intent = Intent(Intent(this, AboutActivity::class.java))
            R.id.settingspage -> intent = Intent(Intent(this, SettingsActivity::class.java))
            R.id.website-> intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.dawsoncollege.qc.ca/sustainable/"))
        }
        Log.d(TAG,"intent "+item.itemId+" starting")
        startActivity(intent)
        return true
    }

    fun onTripLogClick(view: View){
        startActivityForResult(Intent(Intent(this, TripLogActivity::class.java)),tripActivityRequestCode)
        //startActivity(Intent(Intent(this, TripLogActivity::class.java)))
    }

    fun quickHomeTrip(view: View) {
        val intent = Intent(Intent(this, QuickTripActivity::class.java))
        intent.putExtra("tripTo",getString(R.string.tripToHome))
        startActivity(intent)
    }

    fun quickSchoolTrip(view: View) {
        val intent = Intent(Intent(this, QuickTripActivity::class.java))
        intent.putExtra("tripTo",getString(R.string.tripToSchool))
        startActivity(intent)
    }

    fun weatherClick(view: View) {
        startActivity(Intent(Intent(this, WeatherActivity::class.java)))
    }

    fun showCalculatorDialog(view: View){
        val ft = getSupportFragmentManager().beginTransaction()
        val newFragment = Co2CalculatorFragment.newInstance()
        newFragment.show(ft, "dialog")
    }

    fun factsClick(view: View) {
        startActivity(Intent(Intent(this, FactsActivity::class.java)))
    }

    fun onTripLogRemoteClick(view: View){
        startActivity(Intent(Intent(this, TripLogRemoteActivity::class.java)))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == tripActivityRequestCode && resultCode == Activity.RESULT_OK)
        {

            co2Total += data!!.getDoubleExtra("co2",0.0)
            var trees = co2Total/21.7724
            totalCO2ToDate.text = getString(R.string.totalCO2ToDate) + " " + String.format("%.2f",co2Total) + " " + getString(R.string.co2_calculation_response_2)+ " "+ String.format("%.2f",trees) + " " + getString(R.string.trees)
        }
    }

    fun openWeatherActivity(v:View){
        startActivity(Intent(this, WeatherActivity::class.java))
    }
}


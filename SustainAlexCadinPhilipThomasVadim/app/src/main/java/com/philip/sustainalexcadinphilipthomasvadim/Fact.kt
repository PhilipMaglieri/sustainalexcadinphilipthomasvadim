package com.philip.sustainalexcadinphilipthomasvadim


/**
 * Container for data about sustainability fact
 *
 */

class Fact{
    var text : String
    var url : String
    var imageName : String

    constructor() : this("","","")

    constructor(text : String, url:String, imageName : String){
        this.text = text
        this.url = url
        this.imageName = imageName
    }
}

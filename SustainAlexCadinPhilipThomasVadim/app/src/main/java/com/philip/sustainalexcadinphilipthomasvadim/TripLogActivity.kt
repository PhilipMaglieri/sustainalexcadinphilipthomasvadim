package com.philip.sustainalexcadinphilipthomasvadim

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_weather.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection
import kotlin.math.roundToInt

class TripLogActivity : AppCompatActivity() {


    private lateinit var tripViewModel: TripViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var sharedPref : SharedPreferences

    private var tripName : String = ""
    private var travelMode : String = ""
    private var fromLatitude : String = ""
    private var fromLongitude : String = ""
    private var toLatitude : String = ""
    private var toLongitude : String = ""
    private var distance : Double = 0.0
    private var co2 : Double = 0.0
    private var date : String = ""
    private var time : String = ""
    private var reason : String = ""
    private var destination : String = ""
    private var carpool : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trip_log_view)

        sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)
        val adapter = TripLogAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener{
            startActivityForResult(Intent(Intent(this, NewTripActivity::class.java)),newTripActivityRequestCode)
        }

        tripViewModel = ViewModelProvider(this).get(TripViewModel::class.java)
        tripViewModel.allTrips.observe(this, Observer{ trips ->

            trips?.let{adapter.setTrips(it)}
        })
    }

    /**
     *
     *  gets user location and sets certain information from the return result to prepare api call
     *  to get trip information
     *
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "received results")
        if (requestCode == newTripActivityRequestCode && resultCode == Activity.RESULT_OK) {
            data?.getStringArrayListExtra(NewTripActivity.DETAILS)?.let {
                if(it[1].equals("walk/bike")){

                }
                tripName = it[0]
                travelMode = it[1]
                destination = it[2]
                reason = it[3]
                carpool = it[4]
                val dateFormater = SimpleDateFormat("dd/M/yyyy")
                date = dateFormater.format(Date())

                //val timeFormater = SimpleDateFormat("hh:mm")
                //time = timeFormater.format(Date())

                //either school or home
                if(destination.equals("home")) {
                    val split = sharedPref.getString("home",null)!!.split(",")
                    toLatitude = split[0]
                    toLongitude = split[1]
                }
                else {
                    val split = sharedPref.getString("school",null)!!.split(",")
                    toLatitude = split[0]
                    toLongitude = split[1]
                }

                /** val trip = Trip(it[0],it[1],it[2]+"testfromlat",it[2]+"testfromlong",
                it[2]+"testtolat",it[2]+"testtolong",500.0,100.0,
                "testdate","testtime",it[3])

                tripViewModel.insert(trip)* */
            }
        } else {
            Toast.makeText(
                applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG).show()
        }
        //checking if the location permission has been approved
        val hasLocationPermission = ContextCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED
        if(hasLocationPermission){
            Log.d("location", "success")
            getTripWithLocation()
        } else{
            //request location permission if not allowed already
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),1)
        }

    }

    /**
     * gets current location and then calls another functions to call api
     */
    private fun getTripWithLocation (){
        Log.d(TAG, "getTripWithLocation()")
        val locationCallBack : LocationCallback = object : LocationCallback(){
            override fun onLocationResult(locationResult: LocationResult?){
                locationResult ?: return
                for (location in locationResult.locations){
                    getConnectionForAPICall()
                }
            }
        }

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                if(location != null){

                    Log.d(TAG, "Got location")
                    this.fromLatitude = location.latitude.toString()
                    this.fromLongitude = location.longitude.toString()
                    Log.d(TAG,"no input so fail ?")
                    getConnectionForAPICall()

                } else{
                    Log.e(TAG, "Couldn't get location")
                    Toast.makeText(this,getString(R.string.location_error), Toast.LENGTH_SHORT).show()
                    // if location was null, a location update is requested from the framework.
                    fusedLocationClient.requestLocationUpdates(
                        LocationRequest(),
                        locationCallBack,
                        Looper.getMainLooper())
                }
            }
    }

    fun getConnectionForAPICall(  ){
        Log.d(TAG,"getConnectionForAPICall")
        val manager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = manager.activeNetworkInfo
        val login = "https://sustaincadinthomas.herokuapp.com/api/auth/login"
        val sharedPref = getSharedPreferences("user_info",Context.MODE_PRIVATE)
        val username = sharedPref.getString("email",null)
        val password = sharedPref.getString("password",null)
        val authentication: String = "{ \"email\": \"$username\" , \"password\":\"$password\"}"
        if(info != null && info.isConnected){
            GetTokenFromAPI().execute(login, authentication, "POST","")
        }
        else {
            Log.e(TAG, "internet conncection error")
        }
    }


    /**
     * based on weather activity perhaps transfer to a common class
     * Calls the PHP projects API, retrieves the trip details
     *
     */
    private inner class GetTokenFromAPI : AsyncTask<String, Void, JSONObject>() {

        override fun onPreExecute() {
           /* Toast.makeText(
                applicationContext,
                R.string.trip_loading,
                Toast.LENGTH_LONG).show() */
            Log.d(TAG, "on preExecute() getTokenFromAPI")

        }

        override fun doInBackground(vararg args: String): JSONObject {
            Log.d(TAG, "getTokenFromAPI.doInBackground")

            //last known location of device
           // fromLatitude = args[0]?.latitude.toString()
            //fromLongitude = args[0]?.longitude.toString()

            //val mode = "car"
            // val engine = "diesel"
            //val consumption = "12.3"

            try {
                return downloadFromURL(*args)
            }
            catch (e : IOException){
                return getErrorJSON(R.string.trip_load_error)
            }
            //return getJSONFromAPI("http://jayaghgtracker.herokuapp.com/api/v1/tripinfo?fromlatitude=$fromLatitude&fromlongitude=$fromLongitude&tolatitude=$toLatitude&tolongitude=$toLongitude&mode=$travelMode")
        }

        override fun onPostExecute(result: JSONObject) {
            Log.d(TAG, "onPostExecute getTokenFromAPI ")
            val token = result["access_token"].toString()
            var url : String

            if(travelMode !="car"){
                url = "?fromlatitude=$fromLatitude&fromlongitude=$fromLongitude&tolatitude=$toLatitude&tolongitude=$toLongitude&mode=$travelMode"
            }
            else {
                url = "?fromlatitude=$fromLatitude&fromlongitude=$fromLongitude&tolatitude=$toLatitude&tolongitude=$toLongitude&mode=$travelMode&engine=gasoline&consumption=10.1"
            }
            val infoAPI = "https://sustaincadinthomas.herokuapp.com/api/v1/tripinfo"
            GetTripDetailsFromAPI().execute(infoAPI,url,"GET",token)

        }

    }


    private inner class GetTripDetailsFromAPI : AsyncTask<String,Void,JSONObject>(){
        override fun onPreExecute() {
            Log.i(TAG, "calling trip details API")

        }

        override fun doInBackground(vararg args: String): JSONObject {
            Log.d(TAG,"doInBackground GetTripDetailsFrom ApI")
            try{
                return downloadFromURL(*args)
            }
            catch(e : IOException){
                return getErrorJSON(R.string.trip_load_error)
            }
        }

        override fun onPostExecute(result: JSONObject){
            Log.d(TAG,"onPostExecute GetTripDetailsFrom ApI")
            if(result.has("error")){
                Toast.makeText(
                    applicationContext,
                    R.string.trip_load_error,
                    Toast.LENGTH_LONG).show()
                return
            }
            co2 = result["co2emissions"].toString().toDouble()

            distance = result["distance"].toString().toDouble()
            var distanceInKms = distance/1000

            time = result["traveltime"].toString()
            var timeInMinutes = (time.toDouble()/60)

            if(carpool == "3"){
                co2 /= 3
            }

            val trip = Trip(
                tripName, travelMode,fromLatitude,fromLongitude,toLatitude,toLongitude,
                distanceInKms, co2, date, timeInMinutes.roundToInt().toString(), reason
            )

            tripViewModel.insert(trip)

            co2 += sharedPref.getFloat("co2",0F)

            with(sharedPref.edit()){
                putFloat("co2",co2.toFloat())
                commit()
            }

            giveCO2result()

        }
    }

    /**
     * based on weather activity and CO2 frag perhaps transfer to a common class for
     * data retreival
     * small changes made to corrrespond to this activity
     */
    @Throws(IOException::class)
    private fun downloadFromURL(vararg args: String): JSONObject{
        Log.d(TAG,"downloadFromURL ()")
        var istream: InputStream? = null
        var outStream : OutputStream
        var conn: HttpURLConnection? = null
        var data : String
        val response : Int
        val url : URL
        val urlString : String


        var bytes : ByteArray = "".toByteArray(charset("UTF-8"))
        var method = args[2]
        if (method == "GET") {
            urlString = args[0] + args[1]
        } else {
            urlString = args[0]
            bytes = args[1].toByteArray(charset("UTF-8"))
        }

        try{
            url = URL(urlString)
        }
        catch(e: MalformedURLException){
            return getErrorJSON(R.string.trip_load_error)
        }

        try {
            conn = url.openConnection() as HttpURLConnection

            if(method == "POST"){
                conn.doOutput = true
            }

            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            conn.requestMethod = method
            conn.doInput = true

            if(method == "POST"){
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                conn.addRequestProperty("Content-Length", bytes.size.toString())

                outStream = BufferedOutputStream(conn.outputStream)
                outStream.write(bytes)
                outStream.flush()
                outStream.close()
            }
            else{
                conn.addRequestProperty("Authorization", "Bearer" + args[3])
                conn.connect()
            }



            response = conn.responseCode
            Log.d(TAG, "Server returned: $response")

            // if the response code isn't 200
            if (response != HttpURLConnection.HTTP_OK) {
                return getErrorJSON(R.string.trip_load_error)
            }

            // get the stream for the data from the website
            istream = conn.inputStream
            data = readStream(istream)

            return JSONObject(data)

        } catch (e: IOException) {
            Log.e(TAG, "IO exception in bg")
            e.printStackTrace()
            Log.getStackTraceString(e)
            return getErrorJSON(R.string.trip_load_error)
        } finally {
            if (istream != null) {
                try {
                    istream.close()
                } catch (ignore: IOException) {}

                if (conn != null)
                    try {
                        conn.disconnect()
                    } catch (ignore: IllegalStateException) {}
            }
        }
    }

    /**
     * inspired by co2calculator class same call is made to get trip info
     * reads bytes
     */
    fun readStream(inStream : InputStream?): String{
        Log.d(TAG,"readStream ()")
        var read : Int
        var total = 0
        val buffer = ByteArray(1024)

        val bufferedStream = BufferedInputStream(inStream!!)
        val outputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(outputStream)

        read = 0

        while(read != -1){
            read = bufferedStream.read(buffer)
            if(read>0){
                writer.write(buffer)
                total += read
            }
        }
        writer.flush()

        return outputStream.toString()
    }

    /**
     * sending reply to main activity for updating total CO2
     */
    fun giveCO2result(){
        val replyIntent = Intent()
        replyIntent.putExtra("co2", co2)
        setResult(Activity.RESULT_OK, replyIntent)

    }

    /**
     * taken from weather activity perhaps transfer to a common class for
     * data retreival
     *
     */
    private fun getErrorJSON(error_text_resource : Int) : JSONObject{
        return JSONObject("{\"error\":\""+getText(error_text_resource)+"\"}")
    }

    companion object{
        val TAG = "TripLogActivity"
        const val newTripActivityRequestCode = 1
        const val MY_PERMISSION_ACCESS_COURSE_LOCATION = 1
    }
}
